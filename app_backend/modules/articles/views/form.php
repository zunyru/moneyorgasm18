<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                
            </div>
        </div>

       
        <?php echo form_open_multipart($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-create', 'method' => 'post')) ?>
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="title"></label>
                    <div class="col-sm-7">
                        <label class="m-checkbox m-checkbox--brand">
                            <input <?php echo isset($info->recommend) && $info->recommend=='1' ? "checked" : NULL ?> type="checkbox" name="recommend" valus="1"> รายการแนะนำ
                            <span></span>
                        </label>
                    </div>
                </div>
    
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="title">หัวข้อ <span class="text-danger"> *</span></label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->title) ? $info->title : NULL ?>" type="text" class="form-control m-input " name="title" id="title" required>
                        <label id="name-error-dup" class="error-dup" for="name" style="display: none;">มีชือนี้ในระบบแล้ว.</label>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="slug">Slug <span class="text-danger"> *</span></label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->slug) ? $info->slug : NULL ?>" type="text" class="form-control m-input " name="slug" id="slug"  required>
                        <label id="slug-error-dup" class="error-dup" for="slug" style="display: none;">มี Slug นี้ในระบบแล้ว.</label>
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="slug">ประเภทบทความ <span class="text-danger"> *</span></label>
                    <div class="col-sm-7">
                        <select id="article_categorie_id" name="article_categorie_id" class="form-control m-input  select2" required>
                            <option value="">เลือก</option>
                            <?php
                            if(isset($infotype)):
                                $selected = '';
                                foreach($infotype as $item):
                                    if($info->article_categorie_id == $item->article_categorie_id):
                                        $selected = 'selected';
                                    else:
                                        $selected = '';
                                    endif;
                                    
                                ?>
                                    <option value="<?=$item->article_categorie_id;?>" <?=$selected?>><?=$item->title;?></option>
                                <?php
                                endforeach;
                            
                            endif;
                            ?>
                            
                        </select>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label">วันเวลาแสดงข้อมูล</label>
                    <div class="col-sm-7">
                       <input type="text" name="dateRange" class="form-control" value="<?php echo isset($dateRang) ? $dateRang : NULL ?>"/>
                        <input type="hidden" name="start_date"  value="<?php echo isset($info->start_date) ? $info->start_date : NULL ?>"/>
                        <input type="hidden" name="end_date"  value="<?php echo isset($info->end_date) ? $info->end_date : NULL ?>"/>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="file">ไฟล์ <span class="text-danger"> *</span></label>
                    <div class="col-sm-7">
                        <input id="file" name="file" type="file" data-preview-file-type="text">
                         <input type="hidden" name="outfile" value="<?=(isset($info->file)) ? $info->file : ''; ?>">
                    </div>
                </div> 
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="title">รายละเอียดย่อ</label>
                    <div class="col-sm-7">
                        <textarea name="excerpt" rows="3" class="form-control" id="excerpt"><?php echo isset($info->excerpt) ? $info->excerpt : NULL ?></textarea>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="detail">รายละเอียด</label>
                    <div class="col-sm-7">
                        <textarea name="detail" rows="3" class="form-control summernote" id="detail"><?php echo isset($info->detail) ? $info->detail : NULL ?></textarea>
                    </div>
                </div> 
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" > <h5 class="block">ข้อมูล SEO</h5></label>
                    <div class="col-sm-7">
                       
                    </div>
                </div>
               
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" >ชื่อเรื่อง (Title)</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->metaTitle) ? $info->metaTitle : NULL ?>" type="text" id="" class="form-control" name="metaTitle">
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label">คำอธิบาย (Description)</label>
                    <div class="col-sm-7">
                        <textarea class="form-control" rows="5"  class="form-control" name="metaDescription"><?php echo isset($info->metaDescription) ? $info->metaDescription : NULL ?></textarea>
                    </div>
                </div>   
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label">คำหลัก (Keyword)</label>
                    <div class="col-sm-7">
                        <textarea class="form-control" rows="3"  class="form-control" name="metaKeyword"><?php echo isset($info->metaKeyword) ? $info->metaKeyword : NULL ?></textarea>
                    </div>
                </div> 
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">

                <div class="m-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success m-btn--wide">บันทึก</button>
                    <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                        </div>
                    </div>
                    
                </div>
            </div>
             <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
             <input type="hidden" class="form-control" name="db" id="db" value="articles">
             <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->article_id) ? encode_id($info->article_id) : 0 ?>">
        <?php echo form_close() ?>

        <!--end::Form-->
    </div>
</div>

<script>
    //set par fileinput;
    var required_icon   = true; 
    var file_image      = '<?=(isset($info->file)) ? $this->config->item('root_url').$info->file : ''; ?>';
    var file_id         = '<?=$info->article_id;?>';
    var deleteUrl       = '<?=base_url();?><?=$this->router->class;?>/deletefile/<?=$info->article_id;?>';

</script>
