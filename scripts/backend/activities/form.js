"use strict";

$(document).ready(function() {
  $(".frm-create").validate({
    rules: {
      title: true,
      slug: true
    }
  });

  // create Daterange
  $('input[name="dateRange"]').daterangepicker({
    locale: { cancelLabel: "ยกเลิก", applyLabel: "ตกลง", format: "DD-MM-YYYY" },
    singleDatePicker: true,
    autoUpdateInput: false
  });
  $('input[name="dateRange"]').on("apply.daterangepicker", function(
    ev,
    picker
  ) {
    $(this).val(picker.startDate.format("DD-MM-YYYY"));
    $(this)
      .nextAll()
      .eq(0)
      .val(picker.startDate.format("YYYY-MM-DD"));
    $(this)
      .nextAll()
      .eq(1)
      .val(picker.endDate.format("YYYY-MM-DD"));
  });
  $('input[name="dateRange"]').on("cancel.daterangepicker", function(
    ev,
    picker
  ) {
    $(this).val("");
    $(this)
      .nextAll()
      .eq(0)
      .val("");
    $(this)
      .nextAll()
      .eq(1)
      .val("");
  });

  //load fileinput
  if (method == "create") {
    $("#file").fileinput({
      maxFileCount: 1,
      validateInitialCount: true,
      overwriteInitial: false,
      showUpload: false,
      showRemove: true,
      required: true,
      initialPreviewAsData: true,
      maxFileSize: 2024,
      browseOnZoneClick: true,
      allowedFileExtensions: ["jpg", "png", "gif", "svg"],
      browseLabel: "Picture",
      browseClass: "btn btn-primary btn-block",
      showCaption: false,
      showRemove: true,
      showUpload: false,
      removeClass: "btn btn-danger",
      dropZoneTitle: "Drag & drop picture here …"
    });

    $("#file-multiple").fileinput({
      maxFileCount: 10,
      validateInitialCount: true,
      overwriteInitial: false,
      showUpload: false,
      showRemove: true,
      required: true,
      initialPreviewAsData: true,
      maxFileSize: 2024,
      browseOnZoneClick: true,
      allowedFileExtensions: ["jpg", "png", "gif", "svg"],
      browseLabel: "Picture",
      browseClass: "btn btn-primary btn-block",
      showCaption: false,
      showRemove: true,
      showUpload: false,
      removeClass: "btn btn-danger",
      dropZoneTitle: "Drag & drop picture here …"
    });
  }

  if (method == "edit") {
    $("#file").fileinput({
      initialPreview: [file_image],
      initialPreviewAsData: false,
      initialPreviewConfig: [
        {
          downloadUrl: file_image,
          extra: { id: file_id, csrfToken: get_cookie("csrfCookie") }
        }
      ],
      deleteUrl: deleteUrl + file_id,
      maxFileCount: 1,
      validateInitialCount: true,
      overwriteInitial: false,
      showUpload: false,
      showRemove: true,
      required: required_icon,
      initialPreviewAsData: true,
      maxFileSize: 1024,
      browseOnZoneClick: true,
      allowedFileExtensions: ["jpg", "png", "gif", "svg"],
      browseLabel: "Select Image",
      browseClass: "btn btn-primary btn-block",
      showCaption: false,
      showRemove: true,
      showUpload: false,
      removeClass: "btn btn-danger",
      dropZoneTitle: "Drag & drop icon here …"
    });

    $("#file-multiple").fileinput({
      initialPreview: file_multiple,
      initialPreviewAsData: false,
      initialPreviewConfig: previewConfig_fn(gallery_list),
      maxFileCount: 0,
      validateInitialCount: true,
      overwriteInitial: false,
      showUpload: false,
      showRemove: true,
      required: required_icon,
      initialPreviewAsData: true,
      maxFileSize: 1024,
      browseOnZoneClick: true,
      allowedFileExtensions: ["jpg", "png", "gif", "svg"],
      browseLabel: "Select Image",
      browseClass: "btn btn-primary btn-block",
      showCaption: false,
      showRemove: true,
      showUpload: false,
      removeClass: "btn btn-danger",
      dropZoneTitle: "Drag & drop icon here …"
    });

    function previewConfig_fn(gallery_list) {
      var config_obj = [];
      if (gallery_list.length > 0) {
        $.each(gallery_list, function(key, val) {
          config_obj.push({
            downloadUrl: val.file,
            url: deleteUrl + val.file_id,
            key: val.file_id,
            extra: {
              id: val.file_id,
              csrfToken: get_cookie("csrfCookie")
            }
          });
        });
      }
      console.log(config_obj);
      return config_obj;
    }
  }
});

$(window).on("load", function() {});

$(window).on("scroll", function() {});
