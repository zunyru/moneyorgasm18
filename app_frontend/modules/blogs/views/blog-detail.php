<!--Main layout-->
<main>
  <div class="container ">
    <!--Section: Main info-->
    <section class="mt-5-not-banner wow fadeIn">
      <div class="row">
        <div class="col-md-2 col-sm-6 col text-center">
          <a href="<?=base_url('blogs')?>">
            <img
              class="mb-1"
              src="<?=base_url('template/frontend/img/icon/group00.png');?>"
              alt=""
            />
            <p>All</p>
          </a>
        </div>
        <?php foreach($categories as $cat):?>
        <div class="col-md-2 col-sm-6 col text-center">
          <a href="<?=base_url('blogs').'?category='.$cat->slug?>">
            <img class="mb-1" src="<?=base_url($cat->file)?>" alt="" />
            <p><?=$cat->title;?></p>
          </a>
        </div>
        <?php endforeach; ?>
      </div>
    </section>
    <!--Section: Main info-->
    <section>
      <div class="row">
        <div class="col-md-8 col-sm-12">
          <h1><?=$blogs->title;?></h1>
          <p class="p-date">
            <?=DateEng($blogs->updated_at);?> &nbsp; &nbsp;<?=$blogs->qty_eye;?>
            view
          </p>
          <img
            src="<?=base_url($blogs->file)?>"
            class="img-fluid mt-3"
            style="width: 100%"
            onerror="this.src='<?php echo base_url('template/frontend/img/card.png');?>'"
          />
          <div class="mt-4 mb-4">
            <strong>Share :</strong>
            <div class="sharethis-inline-share-buttons"></div>
          </div>

          <div><?=html_entity_decode($blogs->detail)?></div>
          <div class="mt-4 mb-4">
            <strong>Share :</strong>
            <div class="sharethis-inline-share-buttons"></div>
          </div>
        </div>
        <div class="col-md-4 col-sm-12">
          <!-- facebook -->
          <div
            class="fb-page"
            data-href="https://www.facebook.com/TheConceptWoman/"
            data-tabs="timeline"
            data-width=""
            data-height=""
            data-small-header="false"
            data-adapt-container-width="true"
            data-hide-cover="false"
            data-show-facepile="true"
          >
            <blockquote
              cite="https://www.facebook.com/TheConceptWoman/"
              class="fb-xfbml-parse-ignore"
            >
              <a href="https://www.facebook.com/TheConceptWoman/"
                >Money Orgasm ผู้ประกอบการคิดต่าง 18+</a
              >
            </blockquote>
          </div>
          <!-- end facebook -->
        </div>
      </div>
    </section>
  </div>
</main>
<!--Main layout-->
