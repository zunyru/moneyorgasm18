   <main>
      <div class="container ">
          <section class="mt-5-not-banner">
          <form action="<?=base_url('blogs/search/')?>" method="get">
          <div class="row justify-content-md-center">
            <div class="col-md-auto">
              <div class="form-group has-search">
                <span class="fas fa-search form-control-feedback"></span>
                <input
                  type="text"
                  name="p"
                  class="form-control"
                  placeholder="Content Search"
                />
              </div>
            </div>
          </div>
          </form>
          <hr>
           <h4 class="text-center">Search : <?=$search ?></h4>
            <br>
      </section>
  <!--Section: Main info-->
       <?php if(sizeof($blogs)):?> 
        <section >
          <div class="row ">
            <?php
		          $this->load->view('blog-data', $posts);
            ?>
          </div> 
        </section>
       <?php else:?> 
       <section>
         <hr>
          <div class="mt-5 ">
            <div class="card">
            <div class="card-body">
              <div class="alert alert-warning text-center">
                        <strong>ขออภัยค่ะ !</strong> ไม่มีข้อมูลในหมวดนี้
                      </div>
            </div>
          </div>
          
           
        </div>
        </section>
       <?php endif; ?> 
      </div>
    </main>
    <!--Main layout-->
</div>
</main>