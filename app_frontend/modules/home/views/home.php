    <!-- Full Page Intro -->
    <?php  echo Modules::run('banners/index',''); ?>
    <!-- Full Page Intro -->

    <!--Main layout-->
    <main>
      <div class="container">
        <!--Section: Main info-->
        <section class="mt-5 wow fadeIn">
          <form action="<?=base_url('blogs/search/')?>" method="get">
          <div class="row justify-content-md-center">
            <div class="col-md-auto">
              <div class="form-group has-search">
                <span class="fas fa-search form-control-feedback"></span>
                <input
                  type="text"
                  name="p"
                  class="form-control"
                  placeholder="Content Search"
                />
              </div>
            </div>
          </div>
          </form>

          <!--Grid row-->
          <div class="row text-center mt-5 ">
            <div class="col-md-1 col-sm-0 w-100 "></div>
             <?php foreach($categories as $cat):?>
              <div class="col-md-2 col-sm-6 col text-center">
                <a href="<?=base_url('blogs').'?category='.$cat->slug?>">
              <img class="mb-5" src="<?=base_url($cat->file)?>" alt="" />
              <p><?=$cat->title;?></p>
              </a>
            </div>
            <?php endforeach; ?>
             <div class="col-md-1 col-sm-0 w-100 "></div>
            
          </div>
        </section>
        <!--Section: Main info-->
        <!--Grid row-->
        <div class="row text-center mt-2">
          <div class="col-md-12 mb-12 box-yellow">
            <h4>Becoming a better version of yourself</h4>
            <p>เมื่อคุณคนเดิมตาย คุณคนใหม่จึงปรากฏกลายขึ้นมา</p>
          </div>
        </div>
        
        <?php  echo Modules::run('blogs/home',''); ?>
        
      </div>
    </main>
    <!--Main layout-->