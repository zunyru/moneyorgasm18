<?php 
    if(!empty($info)):
        foreach($info as  $item):
?>
<div class="view">
      <!-- Mask & flexbox options-->
      <div class="d-flex justify-content-center align-items-center ">
        <img class="img-banner pc" src="<?=base_url($item->file)?>" alt="<?=$item->title? $item->title : ''?>"/>
        <img class="img-banner mobile" src="<?=base_url($item->file_mb)?>" alt="<?=$item->title? $item->title : ''?>"/>
      </div>
      <!-- Mask & flexbox options-->
</div>
 <?php 
        endforeach;
    endif;
?>