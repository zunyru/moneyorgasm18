<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller {

	function __construct() {
		parent::__construct();
		
        $this->load->library('facebook');
        $this->load->model('facebook_model');
        $this->load->model('users_model');
        $this->load->library('users_library');
        $this->load->library('uploadfile_library');
    }

    private function seo(){
		$title          = 'ST Developer';
		$robots         = 'ST Developer';
		$description    = 'ST Developer';
		$keywords       = 'ST Developer';
		$meta  			= "<TITLE>".$title."</TITLE>";
		$meta 		   .= "<META name='robots' content='".$robots."'/>";
		$meta		   .= "<META name='description' content='".$description."'/>";
		$meta 		   .= "<META name='keywords' content='".$keywords."'/>";
		$meta 		   .= "<meta property='og:url' content='".site_url()."'/>";
		$meta 		   .= "<meta property='og:type' content='web'/>";
		$meta 		   .= "<meta property='og:title' content='".$title."'/>";
		$meta 		   .= "<meta property='og:description' content='".$description."'/>";
		$meta 		   .= "<meta property='og:image' content='".site_url('images/logo/logo.png')."'/>";
		return $meta;
    }
    
    public function index(){
        if (!empty($this->session->users['UID'])) {
            redirect(base_url(),'refresh');
        }
        $data = array(
            'menu'    => 'login',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'login',
            'footer'  => 'footer',
            'function'=>  array('custom','login'),
		);

        $userData = array();
        
        // Check if user is logged in
        if($this->facebook->is_authenticated()){
            // Get user facebook profile details
            $fbUser = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email,link,gender,picture');

            // Preparing data for database insertion
            $userData['oauth_provider'] = 'facebook';
            $userData['oauth_uid']    = !empty($fbUser['id'])?$fbUser['id']:'';;
            $userData['fname']    = !empty($fbUser['first_name'])?$fbUser['first_name']:'';
            $userData['lname']    = !empty($fbUser['last_name'])?$fbUser['last_name']:'';
            $userData['fullname']    = $userData['fname'].' '.$userData['lname'];
            $userData['email']        = !empty($fbUser['email'])?$fbUser['email']:'';
            // $userData['gender']        = !empty($fbUser['gender'])?$fbUser['gender']:'';
            $userData['oauth_picture']    = !empty($fbUser['picture']['data']['url'])?$fbUser['picture']['data']['url']:'';
            // $userData['link']        = !empty($fbUser['link'])?$fbUser['link']:'';
            $userData['type']        = 'customer';
            
            // Insert or update user data
            $userID = $this->facebook_model->checkUser($userData);
            
            // Check user data insert or update status
            if(!empty($userID)){
                $data['userData'] = $userData;
                // $this->session->set_userdata('users', $userData);
                $users = $this->users_model->find_users_by_user_facebook($userData['oauth_uid']);
                $this->check_coupon($users->user_id);
                $users = $this->users_model->find_users_by_user_facebook($userData['oauth_uid']);
                $this->_set_session($users);


            }else{
               $data['userData'] = array();
            }
            
            // Get logout URL
            $data['logoutURL'] = $this->facebook->logout_url();
        }else{
            // Get login URL
            $data['authURL'] =  $this->facebook->login_url();
        }

        // Load login & profile view
        if ($this->input->get('type')=='popup') {
            $this->load->view('login/popup', $data);
        } else {
            $this->load->view('template/body', $data);
        }
    }

    public function formRegister(){
        if (!empty($this->session->users['UID'])) {
            redirect(base_url(),'refresh');
        }
        $data = array(
            'menu'    => 'login',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'login',
            'footer'  => 'footer',
            'background'  => 'bg',
            'function'=>  array('custom','login'),
        );

        $userData = array();
        
        // Check if user is logged in
        if($this->facebook->is_authenticated()){
            // Get user facebook profile details
            $fbUser = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email,link,gender,picture');

            // Preparing data for database insertion
            $userData['oauth_provider'] = 'facebook';
            $userData['oauth_uid']    = !empty($fbUser['id'])?$fbUser['id']:'';;
            $userData['fname']    = !empty($fbUser['first_name'])?$fbUser['first_name']:'';
            $userData['lname']    = !empty($fbUser['last_name'])?$fbUser['last_name']:'';
            $userData['fullname']    = $userData['fname'].' '.$userData['lname'];
            $userData['email']        = !empty($fbUser['email'])?$fbUser['email']:'';
            // $userData['gender']        = !empty($fbUser['gender'])?$fbUser['gender']:'';
            $userData['oauth_picture']    = !empty($fbUser['picture']['data']['url'])?$fbUser['picture']['data']['url']:'';
            // $userData['link']        = !empty($fbUser['link'])?$fbUser['link']:'';
            $userData['type']        = 'customer';
            $userData['active']        = 1;
            // Insert or update user data
            $userID = $this->facebook_model->checkUser($userData);
            
            // Check user data insert or update status
            if(!empty($userID)){
                $data['userData'] = $userData;
                // $this->session->set_userdata('users', $userData);
                $users = $this->users_model->find_users_by_user_facebook($userData['oauth_uid']);
                $this->check_coupon($users->user_id);
                // $users = $this->users_model->find_users_by_user_facebook($userData['oauth_uid']);
                $this->_set_session($users);

            }else{
               $data['userData'] = array();
            }
            
            // Get logout URL
            $data['logoutURL'] = $this->facebook->logout_url();
        }else{
            // Get login URL
            $data['authURL'] =  $this->facebook->login_url();
        }

        // Load login & profile view
        
        $this->load->view('template/body', $data);
        
    }

    private function _set_session($users){
        //Login Success
        if(empty($users->oauth_picture) && empty($users->file) ){
            $file='images/users/'.rand(1,10).'.png';
        }else{
            $file=$users->file;
        }
        $set_session =  array(
            'UID' => $users->user_id,
            'oauth_uid' => $users->oauth_uid,
            'Username' => $users->username,
            'Name' => $users->fname.' '.$users->lname, 
            'fullname' => $users->fullname, 
            'couponCode' => $users->couponCode,
            'Imgprofile' => $file,
            'ImgprofileFace' => $users->oauth_picture,
            'UserType' => $users->type,
        );

        $this->session->set_userdata('users',$set_session);
    }

    private function check_log($username,$password,$remember=FALSE){
        # code...
        $users = $this->users_library->login(
            $username,
            $password,
            $remember
        );
        //Log
        // $this->logs_library->save_log($username,$this->class,$this->method,$users['message']);
        if($users['status'] === 'success'){
            //Go to  dashboard
            // redirect(base_url('dashboard'),'refresh');
            // $users = $this->session->users;
            // $users['user_id'] = $users['UID'];
            // $users['fullname'] = $users['fullname'];

            // redirect(base_url('login/profile/?t=yes0'),'refresh');
            $users = $this->users_model->find_users_by_user($username);
             $this->_set_session($users);
            redirect_back();
        }elseif ($users['status'] === 'warning') {
        
            $this->session->set_flashdata('status',$users['status']);
            $this->session->set_flashdata('message',$users['message']);
            //Go to  users/login
            redirect(base_url('login/?t=yes1'),'refresh');
        }else{
            $this->session->set_flashdata('status',$users['status']);
            $this->session->set_flashdata('message',$users['message']);
            //Go to  users/login
            redirect(base_url('login/?t=yes2'),'refresh');
        }
    }

    public function url_login_face(){
        
         return  $this->facebook->login_url();;
    }

    public function check_login_modal(){
        $input=$this->input->post();
        $users = $this->users_library->login(
            $input['email'],
            $input['password'],
            $input['remember']
        );
        
        if($users['status'] === 'success'){
             $this->check_coupon($this->session->users['UID']);
             $users = $this->users_model->find_users_by_user($input['email']);
             $this->_set_session($users);
             $resp_msg = array('info_txt'=>"success",'msg'=>'','msg2'=>''); 
        }elseif ($users['status'] === 'warning') {
             $resp_msg = array('info_txt'=>"error",'msg'=>'รหัสผ่านไม่ถูกต้อง','msg2'=>'');
        }else{
             $resp_msg = array('info_txt'=>"error",'msg'=>'รหัสผ่านไม่ถูกต้อง','msg2'=>'');
        }

         echo json_encode($resp_msg);
         return false;
    }

    public function login_user() {
        $input = $this->input->post(null, true);

        $userData['email'] = !empty($input['email'])?$input['email']:'';
        $userData['password'] = !empty($input['password'])?$input['password']:'';
        $userData['remember'] = !empty($input['remember'])?$input['remember']:'';

        $this->check_log($userData['email'], $userData['password'], $userData['remember']);
        // redirect(base_url('login/?t=yes0'),'refresh');
    }

    public function register() {

        $input = $this->input->post(null, true);

        // print"<pre>";
        // print_r($input);
        // exit();
        
        $salt = $this->users_library->salt();
        $password = $this->users_library->hash_password($input['password'], $salt);
        $userData['fullname'] = !empty($input['fname'])? $input['fname'].' '.$input['lname'] : '';
        $userData['fname'] = !empty($input['fname'])?$input['fname']:'';
        $userData['lname'] = !empty($input['lname'])?$input['lname']:'';
        $userData['phone'] = !empty($input['phone'])?$input['phone']:'';
        $userData['username'] = !empty($input['email'])?$input['email']:'';
        $userData['email'] = !empty($input['email'])?$input['email']:'';
        $userData['salt'] = $salt;
        $userData['password'] = $password;
        $userData['type'] = 'customer';
        $userData['active']        = 1;
        $userData['created_at'] = db_datetime_now();
        $userData['updated_at'] = db_datetime_now();

        $is_profile = !empty($input['is_profile'])? $input['is_profile']: 0;
        
        if( $is_profile == 0){
            $profile = array(
                'uploads/users/default/profile_1.png',
                'uploads/users/default/profile_2.png',
                'uploads/users/default/profile_3.png',
                'uploads/users/default/profile_4.png',
                'uploads/users/default/profile_5.png',
            );
            $random_profile = array_rand($profile,1);

            $file='images/users/'.rand(1,10).'.png';
            $userData['file'] = $file;//$profile[$random_profile];
        }else{
            $path                = 'users';
            $upload = $this->uploadfile_library->do_upload('file',TRUE,$path);
            
            $file = '';
            if(isset($upload['index'])){
                $file = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
                $outfile = $input['outfile'];
                if(isset($outfile)){
                    $this->load->helper("file");
                    unlink($outfile);
                }
                $userData['file'] = $file;
            }
        }

        $InsertUser = $this->users_model->register_user($userData);
        
        if ($InsertUser) {
            $this->check_coupon($InsertUser);
            $this->check_log($input['email'], $input['password'], NULL);
        }
    }

    public function update_user() {

        $input = $this->input->post(null, true);
        $id = $input['id'];

        $userData['fname'] = !empty($input['fname'])?$input['fname']:'';
        $userData['lname'] = !empty($input['lname'])?$input['lname']:'';
        $userData['fullname'] = !empty($input['fname'])?$input['fname']:''.' '.!empty($input['lname'])?$input['lname']:'';
        $userData['phone'] = !empty($input['phone'])?$input['phone']:'';

        $updateUser = $this->users_model->update_user($id, $userData);
        
        if ($updateUser==true) {
            redirect(base_url('login/profile'),'refresh');
        }
    }

    public function update_password() {

        $input = $this->input->post(null, true);
        $id = $input['id'];

        $salt = $this->users_library->salt();
        $password = $this->users_library->hash_password($input['password'], $salt);
        $userData['password'] = $password;
        $userData['salt'] = $salt;

        $updateUser = $this->users_model->update_user($id, $userData);
        
        if ($updateUser==true) {
            redirect(base_url('login/profile'),'refresh');
        }
    }

    public function update_password_check() {
        $input = $this->input->post(null, true);
        // var_dump($input);
        $input['user_id'] = $input['id'];
        $input['oldPassword'] = $input['oldPassword'];
        $query = $this->db->where('user_id', $input['user_id'])->get('users');
        $row = $query->row(); 
        if($this->users_library->hash_password($input['oldPassword'],$row->salt) !== $row->password){
            $rs = FALSE;
        } else {
            $rs = TRUE;
           
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($rs));
    }

    public function check_password() {
        $input = $this->input->post();
        $input['user_id'] = decode_id($input['id']);
        $row = $this->users_model->check_passwords($input);
      
        if($this->users_library->hash_password($input['oldPassword'],$row->salt) !== $row->password){
            echo 'no';
        } else {
            echo 'yes';
        }
    }

    public function profile()
	{
        if (empty($this->session->users['UID'])) {
            redirect(base_url('home'),'refresh');
        }
        $data = array(
            'menu'    => 'login',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'profile',
            'footer'  => 'footer',
            'background'    => 'bg',
            'function'=>  array('custom','login','login_profile'),
            // 'function'=>  array('custom','login'),
        );

        if (!empty($this->session->users['oauth_uid'])) {
            $find_users = $this->users_model->find_users_by_user_facebook($this->session->users['oauth_uid']);

        }else{
            $find_users = $this->users_model->find_users_by_user($this->session->users['Username']);
        
            $salt = $this->users_library->salt();
            $password = $this->users_library->hash_password($find_users->password, $salt);
    
            $data['password'] = $password;    
        }
        $data['Users'] = $find_users;

        $this->load->view('template/body', $data);
    }
    

    public function Logout()
	{
		$usename = $this->session->userdata('users');
		//Log
		// $this->logs_library->save_log($usename['Username'],$this->class,$this->method,'Logout : Logouted');

		delete_cookie('username');
		delete_cookie('salt');

        $this->session->unset_userdata('users');
         // destory session
        $this->session->sess_destroy();
        $this->facebook->destroy_session();
		redirect(base_url(),'refresh');
    }

    public function check_username()
    {
        $input = $this->input->post();
        //print_r($input);exit();
        $input['recycle'] = array(0,1);
        $info = $this->users_model->get_rows($input);
//        arrx($info);
        if ( $info->num_rows() > 0 ) {
            if ($input['mode'] == 'create') {
                $rs = FALSE;
            } else {
                $row = $info->row();
                if ($row->user_id == decode_id($input['id'])) {
                    $rs = TRUE;
                } else {
                    $rs = FALSE;
                }
            }
        } else {
            $rs =  TRUE;
        }
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($rs));
    }

     private function check_coupon($user_id)
    {
        
        
        $input['user_id'] = $user_id;
        $info = $this->users_model->get_rows($input)->row();
        if ( $info->couponCode =="") {         
           $chars = "123456789ABCDEFGHIJKLMNPQRSTUVWXYZ";
            $res = "";
            for ($i = 0; $i < 7; $i++) {
                $res .= $chars[mt_rand(0, strlen($chars)-1)];
            }

            $v['couponCode']=$res.$user_id;
            $this->users_model->update_user($user_id,$v);
        } 
    }
    
}
