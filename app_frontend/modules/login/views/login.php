<?php if(!empty($authURL)){ ?>
<div class="container">
    <div class="background-image">
        <div class="row">
            <div class="offset-sm-2 col-sm-8">
                <div class="my-5">
                    <div class="">
                        <h2 class="title text-center">สมัครสมาชิก</h2>
                        <form id="RegisterForm" class="form-signin" action="<?=site_url('login/register');?>"
                            method="post" enctype="multipart/form-data">
                            <div class="form-label-group">
                                <label for="fname">ชื่อ</label>
                                <input type="text" id="fname" name="fname" class="form-control" placeholder="ชื่อ">
                            </div>
                            <div class="form-label-group">
                                <label for="lname">นามสกุล</label>
                                <input type="text" id="lname" name="lname" class="form-control" placeholder="นามสกุล">
                            </div>
                            <div class="form-label-group">
                                <label for="phone">เบอร์โทร</label>
                                <input type="text" id="phone" name="phone" class="form-control"
                                    placeholder="เบอร์โทร">
                            </div>
                            <div class="form-label-group">
                                <label for="email">Email</label>
                                <input type="email" id="email" name="email" class="form-control" placeholder="Email">
                            </div>
                            <div class="form-label-group">
                                <label for="password">รหัสผ่าน</label>
                                <input type="password" id="password" name="password" class="form-control"
                                    placeholder="รหัสผ่าน">
                            </div>
                            <div class="form-label-group">
                                <label for="confirm_password">ยืนยันรหัสผ่าน</label>
                                <input type="password" id="confirm_password" name="confirm_password"
                                    class="form-control" placeholder="ยืนยันรหัสผ่าน">
                            </div>
                            <div class="form-label-group">
                                <label class="m-checkbox m-checkbox--brand">
                                    <input type="checkbox" name="is_profile" class="icheck" id="icheck-profile"
                                        value="1"> ต้องการอัพโหลดโปรไฟล์
                                </label>
                            </div>
                            <div id="profile-img" class="form-label-group d-none">
                                <label for="file">ไฟล์</label>
                                <input id="file" name="file" type="file" data-preview-file-type="text">
                            </div>

                            <div class="form-label-group mt-2">
                                <label class="m-checkbox m-checkbox--brand">
                                    <input type="checkbox" name="" class="icheck" id="is_Condition" value="" required>
                                        <b id="text_Condition" class="text-danger">ยอมรับเงื่อนไข </b>
                                        <a href="#"><u class="text-info">อ่านนโยบาย</u></a>
                                </label>
                            </div>
                            <button class="btn btn-lg btn-danger btn-block text-uppercase"
                                type="submit">ลงทะเบียน</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php 
}else{
    redirect_back();
 } ?>