<!-- Navbar -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark scrolling-navbar">
      <div class="container">
        <!-- Brand -->
        <a
          class="navbar-brand"
          href="<?php echo base_url();?>"
        >
          <img src="<?=base_url('template/frontend/img/logo.png');?>" alt="moneyorgasm18" />
        </a>

        <!-- Collapse -->
        <button
          class="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Links -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <!-- Left -->
          <ul class="navbar-nav ml-auto">
            <li class="nav-item <?=$menu == 'home' ? 'active' : '';?>">
              <a class="nav-link" href="<?php echo base_url();?>"><strong>Home</strong> </a>
            </li>
            <li class="nav-item <?=$menu == 'aboutus' ? 'active' : '';?>">
              <a
                class="nav-link"
                href="<?php echo base_url('aboutus');?>"
                ><strong>About Us</strong></a
              >
            </li>
            <li class="nav-item  <?=$menu == 'products' ? 'active' : '';?>">
              <a
                class="nav-link"
                href="<?php echo base_url('products');?>"
                ><strong>Products and services</strong></a
              >
            </li>
            <li class="nav-item <?=$menu == 'blogs' ? 'active' : '';?>">
              <a
                class="nav-link"
                href="<?php echo base_url('blogs');?>"
                ><strong>Blog</strong></a
              >
            </li>
            <li class="nav-item <?=$menu == 'contact' ? 'active' : '';?>">
              <a
                class="nav-link"
                href="<?php echo base_url('contact');?>"
                ><strong>Contact Us</strong></a
              >
            </li>
          </ul>

          <!-- Right -->
          <ul class="navbar-nav nav-flex-icons">
            <li class="nav-item">
              <a
                href="https://www.facebook.com/TheConceptWoman/"
                class="nav-link"
                target="_blank"
              >
                <img src="<?=base_url('template/frontend/img/icon/facebook.png');?>" />
              </a>
            </li>
            <li class="nav-item">
              <a
                href="#"
                class="nav-link"
                target="_blank"
              >
                <img src="<?=base_url('template/frontend/img/icon/youtube.png');?>" />
              </a>
            </li>
            <li class="nav-item">
              <a
                href="tel:0988297420"
                class="nav-link"
                target="_blank"
              >
                <img src="<?=base_url('template/frontend/img/icon/phone.png');?>" />
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
<!-- Navbar -->