<footer class="page-footer text-center font-small mt-4  wow fadeIn ">
  <!--Call to action-->
  <div class="container">
  <div class="pt-4">
    <div class="row ml-0 mr-0">
      <div class="col-md-4 col-sm-12">
        <img src="<?=base_url('template/frontend/img/logo-footer.png');?>" />
      </div>
      <div class="col-md-7 col-sm12">
        <div class="col-12">
          <div class="row text-left txt-normal">
            <div class="offset-1"></div>
            <div class="col-md-2 col-sm-4"><a href="<?php echo base_url();?>"><strong>Home</strong></a></div>
            <div class="col-md-2 col-sm-4"><a href="<?php echo base_url('aboutus');?>"><strong>About Us</strong></a></div>
            <div class="col-md-2 col-sm-4"><a href="<?php echo base_url('blogs');?>"><strong>Blog</strong></a></div>
            <div class="col-md-3 col-sm-4"><a href="<?php echo base_url('products');?>"><strong>Products and services</strong></a></div>
            <div class="col-md-2 col-sm-4"><a href="<?php echo base_url('contact');?>"><strong>Contact Us</strong></a></div>
          </div>
        </div>
        <div class="col-12 mt-3">
          <div class="row text-left txt-normal">
            <div class="offset-1"></div>
            <div class="col-md-4 col-sm-4">
              <strong>Contact Us :</strong>
              <a href="https://www.facebook.com/TheConceptWoman/" target="_blank">
              <img class="f-socail"
                src="<?=base_url('template/frontend/img/icon/facebook.png');?>"
              />
              </a>
               <a href="#" target="_blank">
              <img class="f-socail"
                src="<?=base_url('template/frontend/img/icon/youtube.png');?>"
              />
             </a>
            </div>
            <div class="col-md-3 col-sm-4"><strong>Tel : 0988297420</strong></div>
            <div class="col-md-4 col-sm-12">
              <strong>Email : Suphitsara.ta@yahoo.com</strong>
            </div>
          </div>
        </div>
      </div>
      <div class="offset-1"></div>
    </div>
  </div>
  <!--/.Call to action-->

  <hr class="my-3 ml-5 mr-5" />
  <!--Copyright-->
  <div class=" pb-3 txt-normal">
    <strong>© 2016 Moneyorgasm18+. All Rights Reserved. By Sansu Sa ma air</strong>
  </div>
  <!--/.Copyright-->
  </div>
</footer>
