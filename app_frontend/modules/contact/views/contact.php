<!--Main layout-->
<main>
  <div class="container">
    <!--Section: Main info-->
    <section class="mt-5-not-banner ">
      <div class="row">
        <div class="col-md-6 col-sm-6">
          <h5>Money Orgasm ONLINE CONTACT CENTER</h5>
          <p class="p-text">(ทุกวันเวลา 9:00-20:00 น.)</p>
          <div class="mt-5">
            <img
              class="d-inline"
              src="<?=base_url('template/frontend/')?>/img/icon/icon01.png"
              alt=""
            />
            <div class="d-inline position-absolute ml-3">
              suphitsara.ta@yahoo.com [ติดต่องาน]<br />
              plengpage2019@gmail.com [สั่งซื้อสินค้าและบริการ]
            </div>
          </div>
          <div class="mt-4">
            <a href="https://www.facebook.com/messages/t/TheConceptWoman">
              <img
                class="d-inline"
                src="<?=base_url('template/frontend/')?>/img/icon/icon02.png"
                alt=""
              />
              <div class="d-inline position-absolute ml-3 mt-3">
                m.me/TheConceptWoman
              </div>
            </a>
          </div>
          <div class="mt-4">
            <a href="http://line.me/ti/p/~PlengXpy">
              <img
                class="d-inline"
                src="<?=base_url('template/frontend/')?>/img/icon/icon03.png"
                alt=""
              />
              <div class="d-inline position-absolute ml-3 mt-3">
                http://line.me/ti/p/~PlengXpy
              </div>
            </a>
          </div>
          <div class="mt-4">
            <a href="tel:0988297420">
              <img
                class="d-inline"
                src="<?=base_url('template/frontend/')?>/img/icon/icon04.png"
                alt=""
              />
              <div class="d-inline position-absolute ml-3 mt-3">
                0988297420
              </div>
            </a>
          </div>
          <section class="mt-4 card-contact">
            <form
              id="contact-form"
              name="contact-form"
              action="<?=base_url('contact/mail')?>"
              method="POST"
            >
              <div class="card">
                <div class="card-body">
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-2 col-form-label"
                      >Name</label
                    >
                    <div class="col-sm-10">
                      <div class="col-sm-10">
                        <input
                          type="text"
                          class="form-control"
                          name="name"
                          required
                        />
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 col-form-label"
                      >Email</label
                    >
                    <div class="col-sm-10">
                      <div class="col-sm-10">
                        <input
                          type="email"
                          class="form-control"
                          name="email"
                          required
                        />
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-2 col-form-label"
                      >Tel</label
                    >
                    <div class="col-sm-10">
                      <div class="col-sm-10">
                        <input
                          type="text"
                          class="form-control"
                          name="tel"
                          required
                        />
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-2 col-form-label"
                      >Detail</label
                    >
                    <div class="col-sm-10">
                      <div class="col-sm-10">
                        <textarea
                          name="message"
                          rows="5"
                          class="form-control"
                          required
                        ></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-12 text-center">
                      <button class="btn btn-primary mo " type="submit">
                        Send
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </section>
        </div>
        <div class="col-md-6 col-sm-6">
          <img
            class="img-fluid"
            src="<?=base_url('template/frontend/')?>/img/content/plang05.png"
            alt=""
          />
        </div>
      </div>
    </section>
    <!--Section: Main info-->
  </div>
</main>
<!--Main layout-->
