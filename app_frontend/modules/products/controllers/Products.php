<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends MX_Controller {
    
    private $perPage = 3;

    public function __construct() 
    {
        parent::__construct();
        $this->load->model('products/product_m');
    }

    private function seo($data = ''){

        $title          = isset($data['title']) ?  config_item('siteTitle').' | '.$data['title'] : config_item('siteTitle').' | Products and services';
        $robots         = config_item('siteTitle');
        $description    = isset($data['description']) ?  $data['description'] : config_item('metaDescription');
        $keywords       = isset($data['keywords']) ?  $data['keywords'] : config_item('metaKeyword');
        $img            = isset($data['img']) ?  site_url($data['img']) :  site_url('template/frontend/img/banner/mobile.jpg');
        $url            = isset($data['url']) ?  $data['url'] :  site_url();
        $meta           = "<TITLE>".$title."</TITLE>";
        $meta          .= "<META name='robots' content='".$robots."'/>";
        $meta          .= "<META name='description' content='".$description."'/>";
        $meta          .= "<META name='keywords' content='".$keywords."'/>";
        $meta          .= "<meta property='og:url' content='".$url."'/>";
        $meta          .= "<meta property='og:type' content='website'/>";
        $meta          .= "<meta property='og:title' content='".$title."'/>";
        $meta          .= "<meta property='og:description' content='".$description."'/>";
        $meta          .= "<meta property='og:image' content='".$img."'/>";
        return $meta;
    
    }
    
    public function index()
    {
        $input['length'] = 6;
        $input['start'] = 0;

        $data = array(
            'menu'          => 'products',
            'seo'           => $this->seo(),
            'navbar'        => 'navbar',
            'content'       => 'products',
            'footer'        => 'footer',
            'function'      =>  array('products'),
        );

        $data['num_row'] = $this->product_m->get_num()->num_rows();

        $data['per_page'] = $this->perPage;
        
        $input['active'] =1;
        
        if(!empty($this->input->get("page"))){
           $input['start'] = ceil($this->input->get("page") * $this->perPage);
           $input['length'] = $this->perPage;
           $data['products'] = $this->product_m->get_rows($input)->result();

           $result = $this->load->view('product-data', $data);
           json_encode($result);  
        }else{    
           $data['products'] = $this->product_m->get_rows($input)->result();
           $this->load->view('template/body', $data);
        } 
    }

     public function detail($slug)
    {
        
         $data = array(
            'menu'          => 'products',
            'navbar'        => 'navbar',
            'content'       => 'product-detail',
            'footer'        => 'footer',
            'function'      =>  array('blogs'),
        );

        $data['products'] = $this->product_m->get_product($slug)->row();

        $seo['title'] = $data['products']->metaTitle;
        $seo['description'] = $data['products']->metaDescription;
        $seo['keywords'] = $data['products']->metaKeyword;
        $seo['img'] = $data['products']->file;
        $seo['url'] = site_url('products/detail'.$data['blogs']->slug);

        $data['seo'] = $this->seo($seo);
        
        $this->load->view('template/body', $data);
    }
}
