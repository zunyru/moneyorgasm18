
    <!-- Full Page Intro -->
    <?php  echo Modules::run('banners/products',''); ?>
    <!-- Full Page Intro -->
<!--Main layout-->
    <main>
      <div class="container ">
        
       <?php if(sizeof($products)):?> 
        <section>
          <div class="row mt-5" id="post-data">
            <?php
		          $this->load->view('product-data', $posts);
            ?>
          </div>  
          <!--Grid row place holder-->
             <?php $this->load->view('product-place-holder'); ?> 
          <!--End Grid row place holder-->  
        </section>
       <?php else:?> 
       <section>
         <hr>
          <div class="mt-5 ">
            <div class="card">
            <div class="card-body">
              <div class="alert alert-warning text-center">
                        <strong>ขออภัยค่ะ !</strong> ไม่มีข้อมูลในหมวดนี้
                      </div>
            </div>
          </div>
          
           
        </div>
        </section>
       <?php endif; ?> 
      </div>
    </main>
    <!--Main layout-->
    