<!-- Full Page Intro -->
<?php  echo Modules::run('banners/products',''); ?>
<!-- Full Page Intro -->
<!--Main layout-->
<main>
  <div class="container ">
    <section>
      <div class="row mt-5">
        <div class="col-md-12 col-sm-12">
          <h1><?=$products->title;?></h1>
          <img
            src="<?=base_url($products->file)?>"
            class="img-fluid mt-3"
            style="width: 100%"
            onerror="this.src='<?php echo base_url('template/frontend/img/card.png');?>'"
          />
          <div class="mt-4 mb-4">
            <strong>Share :</strong>
            <div class="sharethis-inline-share-buttons"></div>
          </div>

          <div><?=html_entity_decode($products->detail)?></div>
          <div class="mt-4">
            <a
              href="https://www.facebook.com/messages/t/TheConceptWoman"
              target="_blank"
            >
              <img
                src="<?=base_url('template/frontend/img/chat.png')?>"
                alt=""
              />
            </a>
          </div>
        </div>
      </div>
    </section>
  </div>
</main>
<!--Main layout-->
