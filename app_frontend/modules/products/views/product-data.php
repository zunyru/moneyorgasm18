<input type="hidden" name="num_row" id="num_row" value="<?=$num_row?>">
<input type="hidden" name="prepage" id="prepage" value="<?=$per_page?>">
<?php foreach($products as $item):?>
<div class="col-md-4 mb-4">
    <a href="<?=base_url('products/detail/'.$item->slug)?>">
    <div class="card">
    <img
        class="card-img-top"
        src="<?=base_url($item->file);?>"
        alt="image"
        style="width:100%" onerror="this.src='<?php echo base_url('template/frontend/img/card.png');?>'"/>
    <div class="card-body">
        <?php if(isset($item->article_categorie_id)):?>
        <div class="box-card-category mb-4"><?=$item->c_title?></div>
    <?php endif;?>
        <h4 class="card-title name"><?=$item->title?></h4>
        <p class="card-text">
        <?=$item->excerpt?>
        </p>
        <p class="p-date"><?=DateEng($item->updated_at);?></p>
    </div>
    </div>
    </a>  
</div>
<?php endforeach;?>