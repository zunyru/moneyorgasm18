<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Product_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    

    public function get_product($slug)
    {
        $query = $this->db
        ->where('slug', $slug)
        ->set('qty_eye','qty_eye+1',FALSE)
        ->update('activities');

       $query = $this->db
        ->select('a.*')
        ->from('activities a')
        ->where('a.slug', $slug)
        ->get();

        return $query;
    }

    public function get_num()
    {

        $query = $this->db
        ->select('a.*')
        ->from('activities a')
        ->where('a.active', 1)
        ->get();

        return $query;
    }

    public function get_rows($param) 
    {   
         $this->db->where("(a.start_date <='".date('Y-m-d')."')");
        if ( isset($param['search']) && $param['search'] != "" ) {
            $this->db
            ->group_start()
            ->like('a.title', $param['search'])
            ->or_like('a.excerpt', $param['search'])
            ->or_like('a.detail', $param['search'])
            ->group_end();
        }
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        if ( isset($param['article_id']) ) 
            $this->db->where('a.article_id', $param['article_id']);

        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);
        if ( isset($param['active']) )
            $this->db->where('a.active', $param['active']);
        
        if ( isset($param['recommend']) )
            $this->db->where('a.recommend', $param['recommend']);

        if ( isset($param['slug']) )
            $this->db->where('a.slug', $param['slug']);

        if ( isset($param['relate_id']) )
            $this->db->where_not_in('a.article_id', $param['relate_id']);
 

        $this->db->order_by('a.recommend', 'DESC');
        $this->db->order_by('a.created_at', 'DESC');
        $query = $this->db
        ->select('a.*')
        ->from('activities a')
        ->get();


        return $query;
    }
}