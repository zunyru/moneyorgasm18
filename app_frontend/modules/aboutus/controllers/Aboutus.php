<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aboutus extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('seos/seos_m');
    }


    private function seo(){

        $title          = 'MO18+ | About Us';
        $robots         = config_item('siteTitle');
        $description    = config_item('metaDescription');
        $keywords       = config_item('metaKeyword');
        $img            = site_url('template/frontend/img/banner/mobile.jpg');
        $meta           = "<TITLE>".$title."</TITLE>";
        $meta          .= "<META name='robots' content='".$robots."'/>";
        $meta          .= "<META name='description' content='".$description."'/>";
        $meta          .= "<META name='keywords' content='".$keywords."'/>";
        $meta          .= "<meta property='og:url' content='".site_url()."'/>";
        $meta          .= "<meta property='og:type' content='web'/>";
        $meta          .= "<meta property='og:title' content='".$title."'/>";
        $meta          .= "<meta property='og:description' content='".$description."'/>";
        $meta          .= "<meta property='og:image' content='".$img."'/>";
        return $meta;
    
    }
    
    public function index(){
    	
        $data = array(
            'menu'          => 'aboutus',
            'seo'           => $this->seo(),
            'navbar'        => 'navbar',
            'content'       => 'aboutus',
            'footer'        => 'footer',
            //'function'      =>  array('custom','aboutus'),
		);


        $this->load->view('template/body', $data);
        
    }

}
