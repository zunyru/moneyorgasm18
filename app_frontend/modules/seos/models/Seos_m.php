<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Seos_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_seos_by_display_page($display_page){
        
        $this->db->where('a.display_page', $display_page);
        $this->db->where('a.active', 1);
        $this->db->where('a.recycle', 0);
        $this->db->select('a.seo_id, a.slug, a.title, a.excerpt, a.detail, a.file');
        $query = $this->db->get('seos a');
        return $query;
    }

    public function get_seos_by_seo_id($seo_id){
        
        $this->db->where('a.seo_id', $seo_id);
        $this->db->where('a.active', 1);
        $this->db->where('a.recycle', 0);
        $this->db->select('a.seo_id, a.slug, a.title, a.excerpt, a.detail, a.file');
        $query = $this->db->get('seos a');
        return $query;
    }
   
}
